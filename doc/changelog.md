## Change log ##

* NEW REPOSITORY
* Merged the relativistic mhd modules
* Whistler wave dampening (parameter halldampen) for Hall mhd
* Added Allard-Jan's radiative transfer raytracing routine
* double-checked the glm for srmhd
* Added srmhdiso from Fabien Casse, tested with Orszag-Tang
* Added MP5
* Tracers for hd and mhd
* Merged mhd and mhdiso
* Re-implementation of the glm and divbfixes in mhd

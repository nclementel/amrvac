# Test movstar

\test movstar
\todo Describe what this test does
\todo The test does not run

    Getaux error:          -1 ix^D=           3          15
    Called from: addgeometry
    ERROR for processor           0 :
    -correctaux from smallvalues-----

# Setup instructions

Setup this test case in 2D with

    $AMRVAC_DIR/setup.pl -d=22 -phi=0 -z=2 -g=24,24 -p=hd

# Description

This test has no description yet.


